//
//  ViewController.swift
//  counter
//
//  Created by 田中 賢治 on 2014/06/10.
//  Copyright (c) 2014年 com.ktanaka. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var counterLabel = UILabel()
    var counterInt:Int = 0
                            
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layoutSubViews()
    }
    
    func layoutSubViews(){
        //デザイン用の定数
        let viewBackgroundColor = UIColor(red:0.675432, green: 0.873014, blue: 0.137582, alpha: 1)
        let btnBackgroundColor = UIColor(red:0.935804, green: 0.922321, blue: 0.887794, alpha: 1)
        let btnTitleColor = UIColor(red:0.205214, green: 0.205208, blue: 0.205211, alpha: 1)
        let btnFont = UIFont(name:"AvenirNext-Heavy", size:20)
        let counterFont = UIFont(name:"Futura-CondensedExtraBold", size:130)
        
        //背景色の指定
        self.view.backgroundColor = viewBackgroundColor
        
        //counterLabel
        counterLabel.frame = CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height/2)
        counterLabel.text = "0"
        counterLabel.textColor = UIColor.whiteColor()
        counterLabel.font = counterFont
        counterLabel.textAlignment = NSTextAlignment.Center
        self.view.addSubview(counterLabel)
        
        //countUpBtn
        let countUpBtn:UIButton = UIButton(frame:CGRectMake(200, 400, 50, 50))
        countUpBtn.tag = 1
        countUpBtn.backgroundColor = btnBackgroundColor
        countUpBtn.font = btnFont
        countUpBtn.setTitle("+", forState: UIControlState.Normal)
        countUpBtn.setTitleColor (btnTitleColor, forState: UIControlState.Normal)
        countUpBtn.addTarget(self, action: "onTapButton:", forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(countUpBtn)
        
        //countDownBtn
        let countDownBtn:UIButton = UIButton(frame:CGRectMake(80, 400, 50, 50))
        countDownBtn.tag = 2
        countDownBtn.backgroundColor = btnBackgroundColor
        countDownBtn.font = btnFont
        countDownBtn.setTitle("-", forState: UIControlState.Normal)
        countDownBtn.setTitleColor (btnTitleColor, forState: UIControlState.Normal)
        countDownBtn.addTarget(self, action: "onTapButton:", forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(countDownBtn)
        
        //cleaBtn
        let clearBtn:UIButton = UIButton(frame:CGRectMake(140, 400, 50, 50))
        clearBtn.tag = 3
        clearBtn.backgroundColor = btnBackgroundColor
        clearBtn.font = btnFont
        clearBtn.setTitle("C", forState: UIControlState.Normal)
        clearBtn.setTitleColor (btnTitleColor, forState: UIControlState.Normal)
        clearBtn.addTarget(self, action: "onTapButton:", forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(clearBtn)
    }
    
    func onTapButton(sender:AnyObject) {
        //ボタンのタグでボタンの挙動を判別
        if sender.tag == 1 {
            counterInt++
        } else if sender.tag == 2 {
            counterInt--
        } else {
            counterInt = 0
        }
        
        //ボタンタイトルでボタンの挙動を判別
//        if sender.currentTitle == "+"{
//            counterInt++
//        } else if sender.currentTitle == "-" {
//            counterInt--
//        } else {
//            counterInt = 0
//        }
        
        counterLabel.text = String(counterInt)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}




